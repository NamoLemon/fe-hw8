/*При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, 
то есть все остальные круги сдвигаются влево.
*/

document.querySelector('input').onclick = function () {
	
	//Видалення першої кнопки
	const butDrawCircle = document.getElementById("butt1");
	butDrawCircle.remove();

	//Текст до строки
	const text = document.createElement('p');
	text.textContent = "Напишіть діаметр кола:";
	document.body.append(text);

	//Поле для вводу
	const field = document.createElement('input');
	field.classList.add('type="text"');
	document.body.append(field);
	
	//Створення нової кнопки
	const butDraw = document.createElement("button");
	butDraw.innerHTML = "Намалювати";
	butDraw.classList.add('id="butt2"');
	document.body.append(butDraw);
	
	butDraw.onclick = () => {
		
		// Видалення зайвого
		text.remove();
		field.remove();
		butDraw.remove();

		
		for(let i = 0; i < 10; i++){
			for(let j = 0; j < 10; j++){
				const circle = document.createElement('div');
				circle.className = ("circles" + i + j);
				circle.style.width = field.value + "px";
				circle.style.height = field.value + "px";
				circle.style.backgroundColor = `hsl(${Math.floor(Math.random() *360 )}, 50%, 50%)`;
				circle.style.borderRadius = '50%';
				circle.style.display = "inline-block";

				//Видалення по кліку
				circle.onclick = () => circle.remove();


				document.body.append(circle);

			}const row = document.createElement('br');
			document.body.append(row);
			
		}






	
}
}

